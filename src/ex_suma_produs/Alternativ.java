package ex_suma_produs;

public class Alternativ {
    public static void main(String[] args) {
        int sum = 0, product = 1;
        int contor3 = 0, contor2 = 0;
        int i = 0;

        while (contor3 < 5 || contor2 < 4) {
            if (i % 3 == 0 && contor3 < 5) {
                sum += i;
                contor3++;
            }
            if (i % 2 == 0 && i != 0 && contor2 < 4) {
                product *= i;
                contor2++;
            }
            i++;
        }

        System.out.println("Suma primelor 5 numere divizibile cu 3 este: " + sum);
        System.out.println("Produsul primelor 4 numere divizibile cu 2 excluzand 0 este: " + product);
    }
}


