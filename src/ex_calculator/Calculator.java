package ex_calculator;

public class Calculator {
    // acesta este un constuructpr (default)
    public Calculator(){

    }
    public int aduna(int a, int b) {
        return a + b;
    }

    public int scade(int a, int b) {
        return a - b;
    }

    public int inmulteste(int a, int b) {
        return a * b;
    }

    public double imparte(int a, int b) {
        return (double) a / b;
    }
}
