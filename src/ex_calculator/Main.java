package ex_calculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        readData();
    }

    public static String readData() {
        Calculator calc = new Calculator();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introduceti primul numar:");
        int a = scanner.nextInt();
        System.out.println("Introduceti al doilea numar:");
        int b = scanner.nextInt();

        scanner.nextLine();

        String rezultat = "";
        String operatie;
        do {
            System.out.println("Alege o comanda de mai jos:");
            System.out.println("+ adunare");
            System.out.println("- scadere");
            System.out.println("* inmultire");
            System.out.println("/ impartire");
            System.out.println("Scrie 'stop' pentru a opri programul.");

            operatie = scanner.nextLine();

            if (operatie.equals("stop")) {
                System.out.println("Programul se opreste.");
                break;
            }

            switch (operatie) {
                case "+":
                    rezultat = "Rezultatul adunarii: " + calc.aduna(a, b);
                    break;
                case "-":
                    rezultat = "Rezultatul scaderii: " + calc.scade(a, b);
                    break;
                case "*":
                    rezultat = "Rezultatul inmultirii: " + calc.inmulteste(a, b);
                    break;
                case "/":
                    rezultat = "Rezultatul impartirii: " + calc.imparte(a, b);
                    break;
                default:
                    System.out.println("Semnul operatiei introdus nu este valid.");
                    continue;
            }

            System.out.println(rezultat);

        } while (true);


        scanner.close();
        return rezultat;
    }
}