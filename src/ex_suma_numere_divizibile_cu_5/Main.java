package ex_suma_numere_divizibile_cu_5;

public class Main {
    public static void main(String[] args) {
    // sa se calaculeze suma numerelor divizibile cu 5 din intervalul 0 pana la 50
        int sum = 0;
        for (int i = 0; i <= 50; i++) {
            if (i % 5 == 0) {
                sum += i;
            }
        }
        System.out.println("Suma numerelor divizibile cu 5 in intervalul 0 pana la 50 este: " + sum);
    }
}
