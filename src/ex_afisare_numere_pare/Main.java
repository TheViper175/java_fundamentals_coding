package ex_afisare_numere_pare;

public class Main {
    public static void main(String[] args) {
        // sa se afiseze toate numerele pare naturale pana la 99
        for (int i = 0; i < 100; i += 2) {
            System.out.println(i);
        }
    }
}
